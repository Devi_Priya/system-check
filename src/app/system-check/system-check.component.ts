import { Component, OnInit, NgZone } from '@angular/core';


@Component({
  selector: 'app-system-check',
  templateUrl: './system-check.component.html',
  styleUrls: ['./system-check.component.css']
})
export class SystemCheckComponent implements OnInit {
 public myaudio:any;
 public button:boolean=false;
 public show1:boolean=true;
 public show2:boolean=false;
 public show3:boolean=false;
 private recorder;
 private recStream;
 private mediaRecorder;
 private deviceslist;
 private selectedDevice;
 private Abps;
 private analyserNode;
 private myAudio;
 private context;
 public icon1:boolean=true;
 public icon2:boolean=false;
 public n1:boolean=false;
 public n2:boolean=false;
 public n3:boolean=false;
 constructor(private ngzone:NgZone) { }
  ngOnInit() {
    this.myaudio=new Audio('assets/music/sample.mp3');
    setTimeout(()=>{
      this.initRec();
    },500);
  }

  click(){
    
    this.myaudio.play();

     }
     showNextBtn(){
       this.button=true;
      
     }
     hideNextBtn(){
      this.button=false;
    }
    showNext(id){
      
      this.button=false;
      this.n1=false;
      this.n2=false;
      this.n3=false;
      if(id==1){
this.show1=false;
this.show2=true;
this.show3=false;
document.getElementById("id2").setAttribute("class", "active");
    }
    if(id==2){ 
this.show1=false;
this.show2=false;
this.show3=true;
document.getElementById("id3").setAttribute("class", "active");
    }
  } 
  
  showPrevious(id){
    if(id==1){
this.show1=true;
this.show2=false;
this.show3=false;
document.getElementById("id2").setAttribute("class", "");
  }
  if(id==2){ 
this.show1=false;
this.show2=true;
this.show3=false;
document.getElementById("id3").setAttribute("class", "");
  }
} 
options(id){
  if(id==1){
    this.n1=true;
    this.n2=false;
    this.n3=false;
    
  }
  if(id==2){
    this.n1=false;
    this.n2=true;
    this.n3=false;
   
  }
  if(id==3){
    this.n1=false;
    this.n2=false;
    this.n3=true;
  }
}

startRec(id) {
  this.context.resume().then(() => {
    console.log('Playback resumed successfully');
  });
 
  this.mediaRecorder = new MediaRecorder(this.recStream);
    this.mediaRecorder.start();
    console.log(this.mediaRecorder.state);
    console.log("recorder started");
    this.Abps = this.mediaRecorder.audioBitsPerSecond;
  if(id==1)
  {
    this.icon1=false;
    this.icon2=true;
  }
}
blobToFile(theBlob: Blob, fileName: string): File {
  var b: any = theBlob;
  b.lastModifiedDate = new Date();
  b.name = fileName;
  return <File>theBlob;
}
stopRec(id) {
  var chunks = [];
  var audio = document.createElement('audio');
  this.mediaRecorder.ondataavailable = (e) => {
    chunks.push(e.data);
    // var filelink = this.blobToFile(e.data, "thedata.ogg");
    // var aElement = document.createElement("a");
    // document.body.appendChild(aElement);
    var blob = new Blob(chunks, { 'type': 'audio/ogg; codecs=opus' });
    chunks = [];
    var audioURL = URL.createObjectURL(blob);
    audio.src = audioURL;
    this.myAudio=new Audio(audio.src);
    console.log(this.myAudio);
    audio.controls = true;
    //sdocument.body.appendChild(audio);
  }
  this.mediaRecorder.stop();
  if(id==2)
  {
    this.icon1=true;
    this.icon2=false;
  }
 
}
listen()
{
console.log(this.myAudio);
this.myAudio.play(); 
}    
handleSuccess(stream) {
  var context = new AudioContext();
  this.context = context;
  var source = context.createMediaStreamSource(stream);
  var processor = context.createScriptProcessor(1024, 1, 1);
  var analyser = context.createAnalyser();
  analyser.smoothingTimeConstant = 0.8;
  analyser.fftSize = 1024;
  source.connect(analyser);
  analyser.connect(processor);

  source.connect(processor);
  processor.connect(context.destination);
  
  processor.onaudioprocess = (e) => this.ngzone.run(() => {
    // Do something with the data, i.e Convert this to WAV
    //console.log(e.inputBuffer);
    var array = new Uint8Array(analyser.frequencyBinCount);
    analyser.getByteFrequencyData(array);
    let tot = array.reduce(function (acc, val) { return acc + val; });
    this.analyserNode = tot / array.length;
    console.log(this.analyserNode);

  });

}

initRec() {
  navigator.mediaDevices.getUserMedia({ audio: true })
    .then((stream) => {
      this.handleSuccess(stream);
      console.log(stream);
      this.recStream = stream;
      console.log("Initialized");
      this.getRecSource();
    }).catch(()=>{
      console.log("Caught");
    })
}
getRecSource() {
  navigator.mediaDevices.enumerateDevices().then((devices) => {
    devices = devices.filter((d) => d.kind === "audioinput");
    this.deviceslist = devices;
    console.log(devices);
  }).catch();
}
changeRecSource() {
  navigator.mediaDevices.getUserMedia({
    audio: {
      deviceId: this.selectedDevice
    }
  });

}
showNextBtnmic(){
  this.button=true;
}

hideNextBtnmic(){
  this.button=false;
}

  
  }

